/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "notif.h"

#include <stdio.h>

#include "data_model.h" /* dm_get_xpon_onu_object() */
#include "onu_hal_trace.h"

struct ubus_context* s_ctx = NULL;

static struct blob_buf b;

typedef enum _notif_test_case {
    notif_test_dm_instance_added = 0,
    notif_test_dm_instance_removed,
    notif_test_dm_object_changed_transceiver_one,
    notif_test_dm_object_changed_onu_activation,
    notif_test_omci_reset_mib
} notif_test_case_t;

static const char* notif_test_case_to_string(notif_test_case_t notif) {
    switch(notif) {
    case notif_test_dm_instance_added:                 return "dm:instance-added";
    case notif_test_dm_instance_removed:               return "dm:instance-removed";
    case notif_test_dm_object_changed_transceiver_one: return "dm:object-changed";
    case notif_test_dm_object_changed_onu_activation:  return "dm:object-changed";
    case notif_test_omci_reset_mib:                    return "omci:reset_mib";
    default: break;
    }
    return NULL;
}

void notif_init(struct ubus_context* ctx) {
    s_ctx = ctx;
}

static void send_notif_common(notif_test_case_t notif) {
    when_null_trace(s_ctx, exit, ERROR, "ctx is NULL");

    object_wrapper_t* obj = dm_get_xpon_onu_object();
    when_null_trace(obj, exit, ERROR, "xpon_onu object is NULL");
    when_null_trace(obj->name, exit, ERROR, "xpon_onu object has no name");

    const char* const notification = notif_test_case_to_string(notif);
    when_null_trace(notification, exit, ERROR, "No notification name");

    SAH_TRACE_DEBUG("notification='%s'", notification);

    struct blob_attr* msg = NULL;
    char path[128];
    blob_buf_init(&b, 0);
    switch(notif) {
    case notif_test_dm_instance_added: /* no break */
    case notif_test_dm_instance_removed:
        snprintf(path, 128, "%s.ani.1.transceiver", obj->name);
        blobmsg_add_string(&b, "path", path);
        blobmsg_add_u32(&b, "index", 2);
        msg = b.head;
        break;

    case notif_test_dm_object_changed_transceiver_one:
        snprintf(path, 128, "%s.ani.1.transceiver.1", obj->name);
        blobmsg_add_string(&b, "path", path);
        msg = b.head;
        break;

    case notif_test_dm_object_changed_onu_activation:
        snprintf(path, 128, "%s.ani.1.tc.onu_activation", obj->name);
        blobmsg_add_string(&b, "path", path);
        msg = b.head;
        break;

    case notif_test_omci_reset_mib:
        /* Nothing to do */
        break;

    default:
        break;
    }

    const int rc = ubus_notify(s_ctx, &obj->ubus_obj, notification, msg, -1);
    if(rc) {
        SAH_TRACE_ERROR("ubus_notify() failed: rc=%d", rc);
    }

exit:
    return;
}

void notif_send_dm_instance_added_for_transceiver_two(void) {
    send_notif_common(notif_test_dm_instance_added);
}

void notif_send_dm_instance_removed_for_transceiver_two(void) {
    send_notif_common(notif_test_dm_instance_removed);
}

void notif_send_dm_object_changed_for_transceiver_one(void) {
    send_notif_common(notif_test_dm_object_changed_transceiver_one);
}

void notif_send_dm_object_changed_for_onu_activation(void) {
    send_notif_common(notif_test_dm_object_changed_onu_activation);
}

void notif_send_omci_reset_mib(void) {
    send_notif_common(notif_test_omci_reset_mib);
}

