/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>  /* atoi() */
#include <libubus.h> /* ubus_connect() */
#include <unistd.h>  /* access() */

#include "dbg_if.h"
#include "data_model.h"
#include "notif.h"
#include "onu_hal_macros.h"
#include "onu_hal_trace.h"

static const char* s_ubus_socket_path = "/var/run/ubus/ubus.sock";

static struct ubus_context* s_ctx = NULL;

static void usage(const char* prog) {
    printf("%s -h      : show this help and exit\n", prog);
    printf("%s         : start process with ONU nr = 1\n", prog);
    printf("%s onu_nr  : start process with ONU nr = onu_nr (must be 1 or 2)\n", prog);
    printf("\n");
    printf("Examples:\n");
    printf("%s 2       : advertise xpon_onu.2 on ubus\n", prog);
}

static struct ubus_context* connect_to_ubus(void) {
    struct ubus_context* ctx = NULL;
    if(access(s_ubus_socket_path, F_OK) != 0) {
        s_ubus_socket_path = "/var/run/ubus.sock";
        if(access(s_ubus_socket_path, F_OK) != 0) {
            SAH_TRACE_ERROR("No ubus socket path found");
            return NULL;
        }
    }

    ctx = ubus_connect(s_ubus_socket_path);
    if(!ctx) {
        SAH_TRACE_ERROR("Failed to connect to %s", s_ubus_socket_path);
        return NULL;
    }
    return ctx;
}

int main(int argc, char* argv[]) {

    int onu_nr = 1;
    if(argc == 2) {
        const char* option_one = argv[1];
        if(strncmp(option_one, "-h", 2) == 0) {
            usage(argv[0]);
            return 0;
        } else {
            onu_nr = atoi(option_one);
            if((onu_nr < 1) || (onu_nr > 2)) {
                printf("Error: invalid ONU nr: %d is not in [1, 2]\n", onu_nr);
                return 1;
            }
        }
    }

    SAH_TRACE_INFO("Starting %s with onu_nr=%d", argv[0], onu_nr);

    s_ctx = connect_to_ubus();
    if(!s_ctx) {
        return 1;
    }
    SAH_TRACE_INFO("Connected to %s", s_ubus_socket_path);

    dbg_if_init();

    if(dm_init(s_ctx, onu_nr)) {
        return 1;
    }

    notif_init(s_ctx);

    uloop_init();
    signal(SIGPIPE, SIG_IGN);

    ubus_add_uloop(s_ctx);

    uloop_run();

    SAH_TRACE_INFO("Stopping");
    dbg_if_cleanup();
    dm_cleanup();

    ubus_free(s_ctx);
    uloop_done();

    return 0;
}

