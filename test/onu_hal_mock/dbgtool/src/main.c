/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

// System headers
#include <stdio.h>
#include <string.h> // strlen()
#include <libgen.h> /* basename() */

// Own headers
#include "dbg_commands.h" /* ADD_INSTANCE */
#include "debugclnt.h"

static const char* const SERVER = "/var/run/onu_hal_dbg.sock";

static void usage(const char* name) {
    printf("Usage:\n");
    printf("%s -h\n", name);
    printf("%s <command>\n", name);
    printf("\n");
    printf("Options:\n");
    printf("  %-22s : show this help and exit\n", "-h");
    printf("\n");
    printf("Commands:\n");
    printf("  %-22s : add transceiver.2 and send dm:instance-added notification\n", ADD_INSTANCE);
    printf("  %-22s : remove transceiver.2 and send dm:instance-removed notification\n", REMOVE_INSTANCE);
    printf("  %-22s : change transceiver.1 and send dm:object-changed notification\n", CHANGE_TRANSCEIVER);
    printf("  %-22s : change onu_activation and send dm:object-changed notification\n", CHANGE_ONU_ACTIVATION);
    printf("  %-22s : send omci:reset_mib notification\n", OMCI_RESET_MIB);
}

static void handle_command(const char* cmd) {
    char buf[32];
    snprintf(buf, 32, "%s", cmd);
    dbg_handle_command(SERVER, buf, strlen(buf));
}

int main(int argc, char* argv[]) {
    const char* program = basename(argv[0]);
    if(argc <= 1) {
        usage(program);
        return -1;
    }
    const char* command = argv[1];

    if((strcmp(command, "-h") == 0) ||
       (strcmp(command, "--help") == 0)) {
        usage(program);
        return 0;
    }
    if((strcmp(command, ADD_INSTANCE) == 0) ||
       (strcmp(command, REMOVE_INSTANCE) == 0) ||
       (strcmp(command, CHANGE_TRANSCEIVER) == 0) ||
       (strcmp(command, CHANGE_ONU_ACTIVATION) == 0) ||
       (strcmp(command, OMCI_RESET_MIB) == 0)) {
        handle_command(command);
    } else {
        printf("%s: unknown command\n", command);
        return -1;
    }

    return 0;
}
