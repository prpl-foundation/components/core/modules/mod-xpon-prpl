/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * Define _GNU_SOURCE to avoid following error:
 * error: implicit declaration of function ‘mkstemp’ [-Werror=implicit-function-declaration]
 */
#define _GNU_SOURCE

// Corresponding header
#include "debugclnt.h"

// System headers
#include <errno.h>
#include <stdio.h>  // snprintf(..)
#include <stdlib.h> // mkstemp(..)
#include <string.h> // strlen(..)
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h> // struct sockaddr_un
#include <unistd.h> // unlink(..)

#include "onu_hal_trace.h"


bool dbg_handle_command(const char* server_path, const void* msg, size_t len) {
    bool rv = false;
    int sockfd = 0;
    char client_name[128];

    client_name[0] = 0;

    if(access(server_path, F_OK) != 0) {
        SAH_TRACE_ERROR("server %s does not exist", server_path);
        return false;
    }

    sockfd = socket(AF_LOCAL, SOCK_DGRAM, 0);
    if(sockfd == -1) {
        SAH_TRACE_ERROR("failed to create socket: %s", strerror(errno));
        return false;
    }

    struct sockaddr_un client_addr, server_addr;

    memset(&client_addr, 0, sizeof(client_addr));
    client_addr.sun_family = AF_LOCAL;
    snprintf(client_name, 128, "%s", "/tmp/dbg.clientsock.XXXXXX");
    const int tmp_fd = mkstemp(client_name);
    if(tmp_fd == -1) {
        SAH_TRACE_ERROR("failed to make temporary file: %s", strerror(errno));
        goto exit;
    }

    close(tmp_fd);
    unlink(client_name);

    strcpy(client_addr.sun_path, client_name);

    if(bind(sockfd, (const struct sockaddr*) &client_addr, sizeof(client_addr)) == -1) {
        SAH_TRACE_ERROR("failed to bind client address: %s", strerror(errno));
        goto exit;
    }

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_LOCAL;
    strcpy(server_addr.sun_path, server_path);

    if(sendto(sockfd, msg, len, 0,
              (const struct sockaddr*) &server_addr, sizeof(server_addr)) == -1) {
        SAH_TRACE_ERROR("failed to send msg: %s", strerror(errno));
        goto exit;
    }

    rv = true;

exit:
    if(strlen(client_name)) {
        unlink(client_name);
    }
    if(sockfd > 0) {
        close(sockfd);
    }

    return rv;
}
