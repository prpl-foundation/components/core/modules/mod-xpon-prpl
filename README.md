# XPON prpl module

[[_TOC_]]

## Introduction

### Overview

The XPON prpl module is a PON vendor module loaded by `tr181-xpon`, i.e. the TR-181 XPON manager. It communicates via ubus with one or more prpl PON HAL agents.

`tr181-xpon` has the TR-181 DM as northbound IF. The PON HAL agent has the prpl ubus PON DM has northbound IF. The XPON prpl module in essence acts like a bridge or proxy between those two.

### Relevant links

- [prpl ubus PON DM](https://prplfoundationcloud.atlassian.net/wiki/spaces/PRPLWRT/pages/17598181/Ubus+PON+DM)
- [prpl XPON Manager](https://confluence.prplfoundation.org/display/PRPLWRT/XPON+Manager)
- [TR-181 DM](https://usp-data-models.broadband-forum.org/#sec:current-data-models)
- [tr181-xpon](https://gitlab.com/soft.at.home/xpon/applications/tr181-xpon)
- [ubus](https://openwrt.org/docs/techref/ubus)

### Abbreviations

| Abbreviation | Meaning                               |
| ------------ | ------------------------------------- |
| BBF          | Broadband forum                       |
| DM           | data model                            |
| IF           | interface                             |
| ONU          | Optical Network Unit                  |
| PON          | Passive Optical Network               |


## Architecture

### Overall architecture

The diagram below shows the overall architecture:

```
             Bus (ubus, ...)
                    |
                    | <-- TR-181 DM
                    |
        TR-181 XPON | Manager (tr181-xpon)  
     +----------------------------------------+
     |                                        |
     |                                        |
     |           +-----------------------------+
     |           | prpl XPON vendor module     |
     |           | (mod-xpon-prpl)             |
     +-----------|                             |
                 +-----------------------------+
                           |
                           | ubus
                           |
                           | <--- prpl ubus PON DM
                           |
               +-----------+--------+
               |                    |
               |                    |
       +----------------+     +----------------+
       | PON HAL agent  |     | PON HAL agent  |  ....
       | for ONU 1      |     | for ONU 2      |
       +----------------+     +----------------+
                |                   |
        PON/OMCI stack         PON/OMCI stack
        for ONU 1              for ONU 2

```

`tr181-xpon` has the TR-181 DM as northbound IF. `tr181-xpon` implements the vendor independent part. The vendor specific code is in modules. `mod-xpon-prpl` implements this vendor specific part for a board with a prplOS image with one or more prpl PON HAL agents. A PON HAL agent manages the PON/OMCI stack of a single ONU. It has the prpl ubus PON DM as northbound IF. The TR-181 XPON DM and the prpl ubus PON DM are very similar. `mod-xpon-prpl` acts like a bridge between `tr181-xpon` and one or more PON HAL agents. It does a translation between the TR-181 DM and the prpl ubus PON DM.

E.g. assume a board has 1 ONU with a PON HAL agent to manage it. The agent shows `xpon_onu.1` on ubus. `mod-xpon-prpl` then instructs `tr181-xpon` to show `XPON.ONU.1` in the TR-181 DM. If someone sets `XPON.ONU.1.Enable` to 1, `mod-xpon-prpl` uses ubus to call the method `enable()` on `xpon_onu.1`.

The PON HAL agent is sometimes also called the ONU HAL agent because it manages an ONU.


## Tests

### Unit tests

This component has no unit tests (yet). I tested the code interactively in a Docker container: see next section.

### Howto test in a docker container

See the `README.md` of `tr181-xpon` on how to test `tr181-xpon` and a vendor module in a Docker container with an Ambiorix development image.

This section documents things specific for the `mod-xpon-prpl` module.

I developed a mock for the PON HAL agent. It's in `test/onu_hal_mock/onu_hal_mock`. To run the mock, build `mod-xpon-prpl`, open a shell with root privileges to the container, and run the `onu_hal_mock`.

I usually open different shells with root privileges to the container.

#### Shell 1: start syslog-ng and ubus, monitor logging

```
rm -f /var/log/messages
syslog-ng
ubusd &
tail -f /var/log/messages
```

#### Shell 2: run `onu_hal_mock`

```
cd test/onu_hal_mock/onu_hal_mock
./onu_hal_mock
```

#### Shell 3: run tr181-xpon

```
tr181-xpon
```

#### Shell 4: inspect DMs

Use this shell to query the DMs. To query the TR-181 DM:

```
# ubus-cli
[..]
> XPON.?
XPON.
XPON.ONUNumberOfEntries=1
XPON.ONU.1.
XPON.ONU.1.ANINumberOfEntries=1
XPON.ONU.1.Enable=0
[..]
>
```
To query the DMs on ubus, including the prpl ubus PON DM:

```
# ubus list
XPON
XPON.ONU
XPON.ONU.1
XPON.ONU.1.ANI
XPON.ONU.1.ANI.1
[..]
xpon_onu.1
xpon_onu.1.ani.1
[..]
#
```

#### Shell 5: dbgtool

The folder `test/onu_hal_mock` includes a `dbgtool`. It communicates with the ONU HAL agent via a Unix socket. Run `dbgtool` without arguments (or with argument `-h`) to get help:

```
# ./dbgtool 
Usage:
dbgtool -h
dbgtool <command>

Options:
  -h                     : show this help and exit

Commands:
  add_instance           : add transceiver.2 and send dm:instance-added notification
  remove_instance        : remove transceiver.2 and send dm:instance-removed notification
  change_transceiver     : change transceiver.1 and send dm:object-changed notification
  change_onu_activation  : change onu_activation and send dm:object-changed notification
  omci_reset_mib         : send omci:reset_mib notification
#
```

Its main purpose is to test notifications.

To e.g. test the `dm:instance-added` notification, run:

```
./dbgtool add_instance
```

#### Shell 6: run 2nd `onu_hal_mock`

It's possible to run two ONU HAL agents. In one shell, run `onu_hal_mock`. In another shell, run `onu_hal_mock` with option 2:

```
cd test/onu_hal_mock/onu_hal_mock
./onu_hal_mock 2
```

This starts a 2nd ONU HAL agent showing `xpon_onu.2` on ubus. The idea is to test 2 `xpon_onu` instances, and hence 2 `XPON.ONU` instances.

Note: the `dbgtool` is intented to only be used in a situation with one `onu_hal_mock` running. The `onu_hal_mock` uses 1 path for the Unix socket for its debug interface, and `dbgtool` sends its messages to this single Unix socket.



