/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2025 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/* Related header */
#include "command.h"

/* System headers */
#include <stdlib.h>   /* system() */
#include <sys/wait.h> /* WIFEXITED() */

/* Own headers */
#include "mod_xpon_trace.h"


/**
 * Run a command.
 *
 * @param[in] command: the command to run
 * @param[in] handle_non_zero_exit_status_as_error: if true, log an error and
 *     return false if the command terminates normally with a non-zero exit
 *     status. If false, log a message at debug level and return true if that
 *     happens. The caller can set this parameter to true if the command
 *     returning a non-zero exit status should not be considered an error.
 * @param[in] ret: if the command terminates normally, the function assigns the
 *     exit status of the command to this parameter, else it sets it to -1.
 *
 * @return true on success, else false
 */
bool run_command(const char* command,
                 bool handle_non_zero_exit_status_as_error,
                 int* ret) {
    when_null_trace(command, exit, ERROR, "command is NULL");
    when_null_trace(ret, exit, ERROR, "ret is NULL");
    *ret = -1;

    SAH_TRACEZ_DEBUG(ME, "command = '%s'", command);

    const int rc = system(command);
    when_true_trace(-1 == rc, exit, ERROR,
                    "Failed to execute the command '%s'", command);

    if(WIFEXITED(rc)) {
        const int exit_status = WEXITSTATUS(rc);
        *ret = exit_status;

        if(exit_status == 0) {
            SAH_TRACEZ_DEBUG(ME, "Command terminated normally with return "
                             "value 0");
            return true;
        }
        if(handle_non_zero_exit_status_as_error) {
            SAH_TRACEZ_ERROR(ME, "Command terminated normally with return "
                             "value != 0: %d [%s]", exit_status, command);
            return false;
        }
        SAH_TRACEZ_DEBUG(ME, "Command terminated normally with return "
                         "value != 0: %d [%s]", exit_status, command);
        return true;
    }

    SAH_TRACEZ_ERROR(ME, "Command terminated abnormally [%s]", command);

    if(WIFSIGNALED(rc)) {
        SAH_TRACEZ_ERROR(ME, "Command was terminated with the signal %d [%s]",
                         WTERMSIG(rc), command);
    }
exit:
    return false;
}

